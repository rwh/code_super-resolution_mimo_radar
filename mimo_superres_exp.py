
'''
Super-resolution MIMO radar paper: experiments 
Reinhard Heckel, Jan. 2016
'''

#import numpy as np
import itertools
from numpy import *
import sys as syst
from os.path import dirname
syst.path.append('./SPGL1_python_port')
from spgl1 import *
from spgl_aux import *


#######
# helper functions

def sinc(t):
	if t == 0:
		return 1
	else:
		return sin(pi*t)/(pi*t)

def SNR(x,y): # SNR in dB
	assert x.shape == y.shape
	return 10*log(linalg.norm(x)**2 / linalg.norm(y)**2)/log(10)

def addnoise(y,snr): # snr is in dB
	noisepower = linalg.norm(y)**2 / exp(log(10)*snr/10) 
	n = random.randn(len(y))
	n *= sqrt(noisepower)/linalg.norm(n)
	#print 'added noise with SNR: ', SNR(y,n)
	return y + n

#######

class System: 
	
	def __init__(self,N,NR,NT):
		self.N = N
		self.L = 2*N+1
		self.NR = NR
		self.NT = NT
	
	def random_init_ongrid(s,S,grid,SRF):
		'''
		Generate S grid points uniformly at random on the grid 
		'''
		betas = [float(k)*grid['gcbeta']*SRF for k in random.permutation(grid['Kbeta'])[:S] ]
		taus = [float(k)*grid['gctau']*SRF for k in random.permutation(grid['Ktau'])[:S] ]
		nus = [float(k)*grid['gcnu']*SRF for k in random.permutation(grid['Knu'])[:S] ]
		s.bs = ones(S, dtype='complex')
		#self.bs = random.randn(S) + 1j*random.randn(S)
		#self.bs = self.bs/abs(self.bs)
		s.locations = zip(betas,taus,nus)

	def random_init_diag_spread(self,S,grid,SRF):
		'''
		Generate S points as (gridc_tau*t, gridc_nu*t), t = 0,...,S. 
		'''
		betas = [float(k)*grid['gcbeta']*SRF for k in range(S)]
		taus = [float(k)*grid['gctau'] for k in range(S)]
		nus = [float(k)*grid['gcnu'] for k in range(S)]
		self.bs = ones(S, dtype='complex')
		self.locations = zip(betas,taus,nus)



	def random_init(s,S,betamax,taumax,numax):
		'''
		Generate S points uniformaly at random in [0,betamax] x [0,taumax] x [0,numax]. 
		'''
		betas = random.rand(S)*betamax
		taus = random.rand(S)*taumax
		nus = random.rand(S)*numax
		s.bs = 1*ones(S, dtype='complex')
		#s.bs = random.randn(S) + 1j*random.randn(S)
		#s.bs = s.bs/abs(s.bs)
		s.locations = zip(betas,taus,nus)

	def idealized_response(s,X):
		y = zeros( s.L*s.NR , dtype='complex')
		for (beta,tau,nu),b in zip(s.locations,s.bs):
			for j,x in enumerate(X):
				# build F_tau T_tau x_j
				FTx = zeros(s.L,dtype='complex')
				for p in range(-s.N,s.N+1):
					for ell in range(-s.N,s.N+1):
						FTx[p] += exp(2j*pi*p*nu)*s.__DN( float(p-ell)/s.L - tau)*x[ell]
				for r in range(s.NR):
					y[r*s.L:(r+1)*s.L] += b*exp(2j*pi*beta*(s.NT*r + j))*FTx
		return y

	def __DN(self,t):
		if (t % 1) ==0:
			return 1
		else:
			return sin(pi*self.L*t)/(self.L*sin(pi*t))



class Identifier:
	'''
	The super-resolution radar identifier. The locations are identified by solving a regularized least-squares program
	'''
	def __init__(s,N,NR,NT,grid):
		s.N = N
		s.L = 2*N+1
		s.NR = NR
		s.NT = NT
		s.grid = grid
		# Chose random probing signals with entries i.i.d. uniformly on the unit disc, scaled to have unit norm 
		s.X = []
		for j in range(NT):
			x = random.randn(s.L) + 1j*random.randn(s.L)
			x = 1/sqrt(s.L)*x/abs(x) 
			s.X.append(x)
		
		# Build matrix with partial time-frequency shifts of x 
		s.R = zeros((s.L*s.NR,s.grid['Kbeta']*s.grid['Ktau']*s.grid['Knu']),dtype = 'complex')
		for n1,n2,n3 in itertools.product(range(s.grid['Kbeta']),range(s.grid['Ktau']),range(s.grid['Knu']) ):
			beta = float(n1)*grid['gcbeta']
			tau = float(n2)*grid['gctau']
			nu = float(n3)*grid['gcnu']
			Rcolumn = zeros(s.L*s.NR, dtype='complex')
			for j,x in enumerate(s.X):
				FTxj = s.__Fnu(s.__Ttau(x,tau),nu)
				for r in range(s.NR):
					Rcolumn[r*s.L:(r+1)*s.L] += FTxj*exp(2j*pi*beta*(j+s.NT*r))
			s.R[:, n1 + n2*s.grid['Kbeta'] + n3*s.grid['Kbeta']*s.grid['Ktau']] = Rcolumn  


	def IAAestimate(self,y,S,delta=0.0000001,search='keepdist'):
		'''
		iterative adaptive approach 
		'''
		print 'R.shape:', self.R.shape 
		# initialize
		pim1 = zeros(len(self.R.T),dtype = 'complex') 
		p = zeros(len(self.R.T),dtype = 'complex') 
		for k,r in enumerate(self.R.T): # iterate over columns, compute p_k = |<a_k,y>|/||a_k||^2
			p[k] =  abs( dot(r, conj(y)) / linalg.norm(r)**2 )**2
		ctr = 0
		#while linalg.norm(p - pim1)/linalg.norm(p) > delta and ctr < 30:
		while ctr < 50:
			ctr += 1
			pim1 = array(p)	
			# A diag(p) A^H: 
			RR = np.dot( self.R*p , conj(self.R.T) )
			Rinv = linalg.inv( RR )
			#print 'norm Rinv:', linalg.norm(Rinv), linalg.norm(RR)

			for k,r in enumerate(self.R.T): # iterate over columns
				#p[k] =  abs( dot(conj(r), dot(Rinv,y) ) / dot(conj(r), dot(Rinv,r) ) )**2
				# take real part below to avoid numerical problems
				p[k] =  abs( dot(conj(r), dot(Rinv,y) ) / real(dot(conj(r), dot(Rinv,r))) )**2
				#p[k] =  abs( dot(conj(r), dot(Rinv,y) ) / dot(conj(r), dot(Rinv,r) ) )**2
			#print ctr, ':', linalg.norm(p - pim1)/linalg.norm(p)
			#print p

		#print 'norm Rinv:', linalg.norm(Rinv), linalg.norm(RR)
		#print 'IAA, after', ctr, 'iterations:', linalg.norm(p - pim1), linalg.norm(p)
		
		self.find_positions(S,abs(p),search)



	def estimate(s,y,S,sigma = None,search='keepdist'): 
		'''
		Given an observation y, recover S locations, store the corresponding triplets in self.locations.
		search = 'naive': take as estimates the tf-shifts corresponding to the S largest attenuation factors
		search = 'keepdist': ensure additionally a minimum distance of 1/L or 1/(NRNT) 
		'''
		print 'Start estimating..'
		if sigma==None: # Solve exactly
			s,resid,grad,info = spg_bp(s.R,y)
			print 'solver: BP'
		else: 
			assert(sigma>=0)
			opts = spgSetParms({
				'iterations' :   1000*s.L*s.NR  # Set the maximum number of iterations to a large number (default is 10* .. )
				})
			z,resid,grad,info = spg_bpdn(s.R, y, sigma, opts ) # Call the SPGL1 solver 
		
		s.largest = -sort(-abs(z))[:2*S]
		s.find_positions(S,abs(z),search)

	# find the positions of spices from z; take the indices of the S larges values of z as the recovered indices
	def find_positions(s,S,z,search='keepdist'):
		# find positions of spices
		s.locations = [] # recovered locations
		if search=='naive':
			slargest = argsort(-abs(z))[:S]
			for ind in slargest:
				brec = float( ind % s.grid['Kbeta']) * s.grid['gcbeta']
				trec = float( (ind / s.grid['Kbeta']) % s.grid['Ktau'] ) * s.grid['gctau']
				frec = float( ind / (s.grid['Kbeta']*s.grid['Ktau']) ) * s.grid['gcnu']
				s.locations.append((brec,trec,frec))
		if search=='keepdist':
			# `smart' search for the positions of spices: ensure that detected spices are sufficiently distinct
			largest = argsort(-abs(z))
			for ind in largest:
				brec = float( ind % s.grid['Kbeta']) * s.grid['gcbeta']
				trec = float( (ind / s.grid['Kbeta']) % s.grid['Ktau'] ) * s.grid['gctau']
				frec = float( ind / (s.grid['Kbeta']*s.grid['Ktau']) ) * s.grid['gcnu']
				if(len(s.locations) == 0):
					s.locations.append((brec,trec,frec))
				else:
					notviolated = True
					for beta,tau,nu in s.locations:
						db = abs(beta-brec)
						dt = abs(tau-trec)
						df = abs(nu-frec)
						if max( min(db,1-db)*s.NR*s.NT , min(dt,1-dt)*s.L, min(df,1-df)*s.L ) < 1.0:
							# then minium separation condition violated
							notviolated = False
					if notviolated:
						s.locations.append((brec,trec,frec))
				if(len(s.locations) >= S ):
					break


	def evaluate(s,gtruth,fposdist=1.0):
		'''
		Compute the resolution error, defined as the average of:
		sqrt( L^2(hat tau - tau)^2 + L^2(hat nu - nu)^2 + NR^2NT^2(hat beta - beta)^2 ), 
		where (beta,tau,nu) and (hat beta, hat tau, hat nu) are the original and recovered time-frequency shifts, respectively.
		A false positive is an original tf-shift to which no tf-shifts in the estimated tf-shifts closer than fposdist/L can be found
		'''
		avdist = 0.0
		falsepos = 0
		for (beta,tau,nu) in gtruth: # for each original tf-shift, find the closest estimated tf-shift
			# determin mindist
			mindist = s.L
			for brec,trec,frec in s.locations:
				db = abs(beta-brec)
				dt = abs(tau-trec)
				df = abs(nu-frec)
				dist = sqrt( (min(db,1-db)*s.NR*s.NT)**2 + (min(dt,1-dt)*s.L)**2 + (min(df,1-df)*s.L)**2 )
				if mindist > dist:
					mindist = dist

			if mindist >= fposdist:
				falsepos += 1
				avdist += fposdist / len(gtruth) # so we get some penalty..
			else:
				avdist += mindist / len(gtruth)
		return (avdist,falsepos)
			

	def __Ttau(self,x,tau):
		X = fft.fft(x)
		for k in range(-self.N,self.N+1):
			X[k] *= exp(-2j*pi*tau*k)
		xx = fft.ifft(X)
		return xx

	def __Fnu(self,x,nu):
		for p in range(-self.N,self.N+1):
			x[p] *= exp(2j*pi*p*nu)
		return x

#################################################

######################################################### Tests 


def test_run():
	# Problem parameters
	N = 20
	L = 2*N+1
	NT = 3
	NR = 3
	S = 3
	taumaxc = 2 # by default equal to numax
	taumax = float(taumaxc)/sqrt(L) # tau in [0, taumax], taumax <=1 
	betamax = 1
	SRF = 3
	gridc = 1.0/(SRF*L)
	K =  int(floor(taumax/gridc))
	sys = System(N,NR,NT) 
	grid = {'Kbeta':SRF*NR*NT, 'Ktau':K, 'Knu':K, 'gcbeta':1.0/(SRF*NR*NT),'gctau':gridc,'gcnu':gridc}

	print 'Generate', S, 'random points on the fine grid with SRF', SRF
	sys.random_init_ongrid(S,grid)
	ide = Identifier(N,NR,NT,grid)
	y = sys.idealized_response(ide.X)
	#ide.estimate(yideal,S) # via l1 minimization
	sigma = 0.000000000001
	ide.estimate(y,S,sigma) # via l1 minimization
	(av,fp) = ide.evaluate(sys.locations)

	print 'Idealized response. Resolution error, false positives:', av, fp, '(=0 if perfect recovery on the fine grid)'

	print 'Generate ', S, ' random points off the grid '
	sys.random_init(S,betamax,taumax,taumax)
	print "Original locations:"
	for b,t,f in sys.locations:
		print b,t,f
	sigma = 0.01*linalg.norm(y)/sqrt(L)
	y = sys.idealized_response(ide.X)
	ide.estimate(y,S,sigma)
	print "Estimated locations: "
	for b,t,f in ide.locations:
		print b,t,f
	(av,fp) = ide.evaluate(sys.locations)
	print 'Resolution error, false positives:',av,fp, '(= proportional to', 1.0/SRF, ')'


######################################################### Experiments


# Comparison of the convex programming approach for the super-resolution radar problem to:
# IAA

def noise_comp(S,numit):
	print '----- Experiment with: ', S, numit 
	# Problem parameters
	N = 20
	L = 2*N+1
	NT = 3
	NR = 3
	S = 5
	taumax = 2.0/sqrt(L) # tau in [0, taumax], taumax <=1 
	betamax = 1
	SRF = 3
	gridc = 1.0/(SRF*L)
	K =  int(floor(taumax/gridc))
	grid = {'Kbeta':SRF*NR*NT, 'Ktau':K, 'Knu':K, 'gcbeta':1.0/(SRF*NR*NT),'gctau':gridc,'gcnu':gridc}
	
	snrs = [30,20,10,5,3]
	results = zeros((len(snrs), 2))
	falsepositives = np.zeros((len(snrs),2))
	
	for i,snr in enumerate(snrs):
		print 'SNR: ', snr
		for nit in range(numit):
			# Generate a random system
			sys = System(N,NR,NT) 
			sys.random_init_diag_spread(S,grid,SRF)
			
			ide = Identifier(N,NR,NT,grid)
			y = sys.idealized_response(ide.X)
			y10 = addnoise(y,snr)
			
			### l1-minimization
			sigma = 0.025*linalg.norm(y10)			
			ide.estimate(y10,S,sigma,'naive')
			(av,fp) = ide.evaluate(sys.locations)
			print 'l1-minimization: error and false-positives: ', av, fp
			results[i,0] +=  av/numit
			falsepositives[i,0] += float(fp)/numit
			
			### IAA
			ide.IAAestimate(y10,S,sigma)
			(av,fp) = ide.evaluate(sys.locations)
			print 'IAA: error and false-positives: ', av, fp
			results[i,1] +=  av/numit
			falsepositives[i,1] += float(fp)/numit

	
	results = vstack((snrs, results.T,falsepositives.T )).T
	return results


def figure_comparison_to_IAA(highresolution=False):
	if highresolution:
		numit = 100
		print '--- Generate Data of Figure 3 in high resolution ---'
	else:
		print '--- Generate Data of Figure 3 in low resolution ---'
		numit = 10
	resultsSRF_5 = noise_comp(5,numit)

	savetxt('./dat/resultsSRF_nfp.dat', resultsSRF_5, delimiter=' ')



#######



def experiment_off_the_grid(highresolution=False):
	'''
	support recovery via BP denoising off the grid 
	'''
	# Problem parameters
	N = 20
	L = 2*N+1
	NT = 3
	NR = 3
	S = 5
	taumax = 2.0/sqrt(L) # tau in [0, taumax], taumax <=1 
	betamax = 1
		
	if highresolution: # generate the version in the paper
		print '--- Generate Data of Figure 2 in high resolution ---'
		numit = 20
		SRFrange = range(1,21)
	else: # generate a quick plot
		print '--- Generate Data of Figure 2 in low resolution ---'
		numit = 40
		SRFrange = [1,2,3,4,5,6]
	
	results = np.zeros((len(SRFrange),5))
	falsepositives = np.zeros((len(SRFrange),5))

	for q in range(numit):
		# Generate a random system
		sys = System(N,NR,NT) 
		sys.random_init(S,betamax,taumax,taumax)
		print '--------------- at iteration:', q 
		for j, SRF in enumerate(SRFrange):
			print '--- at SRF:', SRF
			results[j,0] = SRF
			gridc = 1.0/(SRF*L)
			K =  int(floor(taumax/gridc))
			grid = {'Kbeta':SRF*NR*NT, 'Ktau':K, 'Knu':K, 'gcbeta':1.0/(SRF*NR*NT),'gctau':gridc,'gcnu':gridc}
			ide = Identifier(N,NR,NT,grid)

			# noiseless
			y = sys.idealized_response(ide.X)
			sigma = 0.01*linalg.norm(y)/sqrt(SRF*L)
			ide.estimate(y,S,sigma)
			(av,fp) = ide.evaluate(sys.locations)
			results[j,1] += av/numit
			falsepositives[j,1] += float(fp)/numit
			
			# 5dB noise
			snr = 5
			y10 = addnoise(y,snr)
			sigma = 0.05*linalg.norm(y10)
			print 'sigma 10dB: ', sigma
			ide.estimate(y10,S,sigma)
			(av,fp) = ide.evaluate(sys.locations)
			results[j,2] += av/numit
			falsepositives[j,2] += float(fp)/numit
			
			# 10dB noise
			snr = 10
			y20 = addnoise(y,snr)
			sigma = 0.05*linalg.norm(y20)	
			ide.estimate(y20,S,sigma)
			(av,fp) = ide.evaluate(sys.locations)
			results[j,3] += av/numit
			falsepositives[j,3] += float(fp)/numit
			
			# 20dB noise
			snr = 20
			y30 = addnoise(y,snr)
			sigma = 0.05*linalg.norm(y30)
			ide.estimate(y30,S,sigma)
			(av,fp) = ide.evaluate(sys.locations)
			results[j,4] += av/numit
			falsepositives[j,4] += float(fp)/numit
	
	savetxt('./dat/offgrid.dat', results, delimiter=' ')
	print 'results:\n', results
	print 'falspositives:\n', falsepositives 
	return results,falsepositives



