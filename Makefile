

all: fig2 fig3

install:
	git clone https://github.com/drrelyea/SPGL1_python_port.git

fig2:
	python -c 'from mimo_superres_exp import *; experiment_off_the_grid(True)'
	cd ./tex; pdflatex figure2_resolution_error.tex

fig3:
	python -c 'from mimo_superres_exp import *; figure_comparison_to_IAA(True)'
	cd ./tex; pdflatex figure3_cmp_IAA.tex

clean:
	find ./tex/ -type f ! -name '*.tex' -delete
	cd ./dat; rm *.dat

