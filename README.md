
1. Introduction
===============

This folder contains the code to reproduce the numerical results reported in: 

**``Super-resolution MIMO radar''**, by R. Heckel

Specifically, the folder contains code to reproduce Figures 2 and 3 by running the corresponding instructions.
For example, `make fig3` will run the code to generate the data for Figure 3, stores the data in the ./dat folder, and generates the file ./tex/figure3_cmp_IAA.pdf containing the figure. 

2. Requirements
===============

Running the code requires installations of Python, Make, and Pdflatex. 

The code is written in Python. 
To solve the corresponding l_1-regularized least-squares problem, we use the SPGL1 solver, described in: 

E. van den Berg and M. P. Friedlander, "Probing the Pareto frontier for basis pursuit solutions", SIAM J. on Scientific Computing, 31(2):890-912, 2008.

The SPGL1 solver can be obtained from: `git clone https://github.com/drrelyea/SPGL1_python_port.git`


3. Quick start
==============

-	Obtain the SPGL1 solver by running the following command in a shell:
		`git clone https://github.com/drrelyea/SPGL1_python_port.git`
-   Run:
	* `make fig2` 	# to generate Figure 2
	* `make fig3` 	# to generate Figure 3

4. Licence
==========

All files are provided under the terms of the Apache License, Version 2.0, see the included file "apache_licence_20" for details.
